package jack.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Destriptions:
 * Created by weipengjie on 2016/11/25.
 */
public class NewOrderFragment extends LazyLoadFragment {
    private View rootView;
    private TextView tvName;

    private String name;

    /**
     * 实例化一个子类页面
     *
     * @param name
     * @return
     */
    public static NewOrderFragment newInstance(String name) {
        NewOrderFragment fragment = new NewOrderFragment();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (null != parent) {
                parent.removeView(rootView);
            }
        } else {
            rootView = inflater.inflate(R.layout.fragment_page, container, false);
            tvName = (TextView) rootView.findViewById(R.id.tv_name);
        }
        Log.d("NewOrderFragment", "onCreateView");
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        name = bundle.getString("name");
        tvName.setText(name);
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    protected void loadData() {
        Log.d("NewOrderFragment", name + " 加载数据");
    }
}
