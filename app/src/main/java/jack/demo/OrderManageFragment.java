package jack.demo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Destriptions:
 * Created by weipengjie on 2016/11/25.
 */

public class OrderManageFragment extends LazyLoadFragment {

    ViewPager viewPager;
    TabLayout tabLayout;
    private View rootView;
    private ArrayList<String> titleList;

    public static OrderManageFragment newInstance(String name) {
        OrderManageFragment fragment = new OrderManageFragment();
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (null != parent) {
                parent.removeView(rootView);
            }
        } else {
            rootView = inflater.inflate(R.layout.fragment_order_management, container, false);
            ButterKnife.bind(this, rootView);
            initViewPager();
            tabLayout = (TabLayout) rootView.findViewById(R.id.order_management_bottom_tabs);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
            tabLayout.setupWithViewPager(viewPager);
        }
        Log.d("OrderManageFragment", "onCreateView");
        return rootView;
    }

    private void initViewPager() {
        titleList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            titleList.add("page" + i);
        }
        viewPager = (ViewPager) rootView.findViewById(R.id.order_management_viewpager);
        viewPager.setAdapter(new ViewPagerAdapter());
        viewPager.setCurrentItem(0);
    }

    @Override
    protected void loadData() {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        public ViewPagerAdapter() {
            super(getChildFragmentManager());
        }

        @Override
        public Fragment getItem(int position) {
            return NewOrderFragment.newInstance(titleList.get(position));
        }

        @Override
        public int getCount() {
            return titleList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }
    }
}
